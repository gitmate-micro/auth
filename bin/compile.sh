#!/bin/sh

compile () {
    echo "compiling $1"
    for d in $1/proto/*.proto; do
        protoc --proto_path=${GOPATH}/src:. --micro_out=. --go_out=. $d;
        echo compiled: $d;
    done
}

compile srv
