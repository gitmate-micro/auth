#!/bin/sh

dir=`pwd`

build() {
    echo "building $1"
    pushd $dir/$1 > /dev/null
    if [ $ENV -e production ]; then
        CGO_ENABLED=0 go build -a -installsuffix cgo -ldflags '-w' -tags build -o auth-$1
    else
        go build -tags build -o auth-$1
    fi
    echo built: $1/auth-$1
    popd > /dev/null
}

build srv
build web
