# Oauth Service

This is the Oauth service

Generated with

```
micro new gitlab.com/gitmate-micro/oauth --namespace=gitmate.micro --type=srv
```

## Getting Started

- [Configuration](#configuration)
- [Dependencies](#dependencies)
- [Usage](#usage)

## Configuration

- FQDN: gitmate.micro.srv.oauth
- Type: srv
- Alias: oauth

## Dependencies

Micro services depend on service discovery. The default is consul.

```
# install consul
brew install consul

# run consul
consul agent -dev
```

## Usage

A Makefile is included for convenience

Build the binary

```
make build
```

Run the service
```
./oauth-srv
```

Build a docker image
```
make docker
```