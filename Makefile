
GOPATH:=$(shell go env GOPATH)

.PHONY: proto test docker


proto:
	./bin/compile.sh

build: proto
	./bin/build.sh

test:
	go test -v ./... -cover -coverprofile=coverage.txt -tags test

docker:
	docker build . -t oauth-srv:latest
