package main

import (
	"github.com/gorilla/mux"
	"github.com/micro/go-log"
	"github.com/micro/go-web"
)

var r *mux.Router

func init() {
	r = mux.NewRouter()
	r.HandleFunc("/login/{provider}", loginHandler).Methods("GET")
	r.HandleFunc("/authenticate/{provider}", authHandler).Methods("GET")
	r.HandleFunc("/logout", logoutHandler).Methods("GET")
}

func main() {
	service := web.NewService(
		web.Name("gitmate.micro.auth.web"),
		web.Version("latest"),
		web.Handler(r),
	)

	if err := service.Init(); err != nil {
		log.Fatal(err)
	}

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
