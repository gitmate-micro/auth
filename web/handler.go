package main

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/micro/go-log"
	srv "gitlab.com/gitmate-micro/auth/srv/proto"
)

var sessionStore = sessions.NewCookieStore([]byte("something-very-secretive"))

func loginHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	session, err := sessionStore.Get(r, "login-session")
	if err != nil {
		logError(w, http.StatusInternalServerError, err)
		return
	}
	state := getRandomString(32)
	session.Values["state"] = state

	// send a request to the auth service to get authentication url combined
	// with credentials
	client := srv.NewAuthService("gitmate.micro.auth.srv", nil)
	resp, err := client.GetAuthCodeURL(
		context.TODO(),
		&srv.AuthCodeURLRequest{
			Provider: vars["provider"],
			State:    state,
		},
	)
	if err != nil {
		logError(w, http.StatusBadRequest, err)
		return
	}

	// save session before returning response
	session.Save(r, w)

	// redirect to the oauth2 authentication page
	http.Redirect(w, r, resp.Url, http.StatusTemporaryRedirect)
	return
}

func authHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	session, err := sessionStore.Get(r, "login-session")
	if err != nil {
		logError(w, http.StatusInternalServerError, err)
		return
	}

	// verify state is not modified elsewhere
	storedState, ok := session.Values["state"]
	if !ok {
		logError(w, http.StatusBadRequest, err)
		return
	}
	recvdState := r.URL.Query()["state"]
	if len(recvdState) != 1 {
		logError(w, http.StatusBadRequest, fmt.Errorf(
			"no state parameter received"))
		return
	} else if storedState != recvdState[0] {
		logError(w, http.StatusForbidden, fmt.Errorf(
			"stored state does not match with the received state parameter"))
		return
	}

	// verify presence of code
	code := r.URL.Query()["code"]
	if len(code) != 1 {
		logError(w, http.StatusForbidden, fmt.Errorf("no code parameter found"))
		return
	}

	// request for an exchange of tokens
	client := srv.NewAuthService("gitmate.micro.auth.srv", nil)
	resp, err := client.GenerateToken(
		context.TODO(),
		&srv.GenerateTokenRequest{
			Code:     code[0],
			Provider: vars["provider"],
		},
	)
	if err != nil {
		logError(w, http.StatusBadRequest, err)
		return
	}

	// store token in session
	session.Values["token"] = resp.Token.GetAccessToken()
	session.Save(r, w)
	enc, _ := json.Marshal(resp.Token)
	w.WriteHeader(http.StatusOK)
	w.Write(enc)
}

func logoutHandler(w http.ResponseWriter, r *http.Request) {
	session, err := sessionStore.Get(r, "login-session")
	log.Log(session.Values)
	if err != nil {
		logError(w, http.StatusInternalServerError, err)
		return
	}
	_, ok := session.Values["token"]
	if ok {
		delete(session.Values, "token")
	}
	session.Save(r, w)
	http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
}

func logError(w http.ResponseWriter, status int, err error) {
	log.Log(err)
	enc, _ := json.Marshal(&response{status, err.Error()})
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(enc)
}

func getRandomString(length uint) string {
	b := make([]byte, length)
	rand.Read(b)
	return base64.URLEncoding.EncodeToString(b)
}

type response struct {
	StatusCode  int    `json:"status_code"`
	Description string `json:"description"`
}
