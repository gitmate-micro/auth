package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/micro/go-log"
	oauth "gitlab.com/gitmate-micro/auth/srv/proto"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/github"
	"golang.org/x/oauth2/gitlab"
)

var conf map[string]*oauth2.Config

func init() {
	if err := godotenv.Load("production.env", "development.env"); err != nil {
		log.Log(err)
	}

	ghConf := oauth2.Config{
		ClientID:     os.Getenv("GITHUB_CLIENT_ID"),
		ClientSecret: os.Getenv("GITHUB_CLIENT_SECRET"),
		RedirectURL:  os.Getenv("GITHUB_REDIRECT_URL"),
		Scopes:       []string{"user"},
		Endpoint:     github.Endpoint,
	}

	glConf := oauth2.Config{
		ClientID:     os.Getenv("GITLAB_CLIENT_ID"),
		ClientSecret: os.Getenv("GITLAB_CLIENT_SECRET"),
		RedirectURL:  os.Getenv("GITLAB_REDIRECT_URL"),
		Scopes:       []string{},
		Endpoint:     gitlab.Endpoint,
	}

	conf = map[string]*oauth2.Config{
		"github": &ghConf, "gitlab": &glConf,
	}
}

func validateProvider(provider string) bool {
	switch provider {
	case "github", "gitlab":
		return true
	}
	return false
}

// OAuth2 manages OAuth2.0 authentication.
type OAuth2 struct {
	store TokenStore
}

// GetAuthCodeURL returns the login URL encoded with client secrets.
func (s *OAuth2) GetAuthCodeURL(
	ctx context.Context,
	req *oauth.AuthCodeURLRequest,
	res *oauth.AuthCodeURLResponse,
) error {
	if ok := validateProvider(req.GetProvider()); !ok {
		return fmt.Errorf("Unknown provider: %s", req.GetProvider())
	}
	if req.GetState() == "" {
		return fmt.Errorf("Empty state parameter")
	}
	res.Url = conf[req.Provider].AuthCodeURL(req.State)
	return nil
}

// GenerateToken exchanges the code for an access token, stores it and sends it
// through the response.
func (s *OAuth2) GenerateToken(
	ctx context.Context,
	req *oauth.GenerateTokenRequest,
	res *oauth.GenerateTokenResponse,
) error {
	if ok := validateProvider(req.GetProvider()); !ok {
		return fmt.Errorf("Unknown provider: %s", req.Provider)
	}
	if req.GetCode() == "" {
		return fmt.Errorf("Empty code parameter")
	}
	token, err := conf[req.Provider].Exchange(ctx, req.Code)
	if err != nil {
		return err
	}
	go s.storeToken(ctx, token, req.Provider)
	res.Token = &oauth.Token{
		AccessToken:  token.AccessToken,
		Expiry:       token.Expiry.UnixNano(),
		RefreshToken: token.RefreshToken,
		TokenType:    token.TokenType,
	}
	return nil
}

func (s *OAuth2) storeToken(
	ctx context.Context, token *oauth2.Token, provider string,
) error {
	type User struct {
		ID int64 `json:"id"`
	}
	var url string

	client := conf[provider].Client(ctx, token)
	user := &User{}

	switch provider {
	case "github":
		url = "https://api.github.com/user"
	case "gitlab":
		url = "https://gitlab.com/api/v4/user"
	default:
		return fmt.Errorf("Unknown provider: %s", provider)
	}

	resp, err := client.Get(url)
	if err != nil {
		return err
	}
	if err := json.NewDecoder(resp.Body).Decode(user); err != nil {
		return err
	}

	err = s.store.CreateOrUpdate(user.ID, provider, token)
	if err != nil {
		return err
	}
	return nil
}
