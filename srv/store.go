package main

import (
	"time"

	"github.com/jinzhu/gorm"
	"golang.org/x/oauth2"
)

type TokenStore interface {
	CreateOrUpdate(int64, string, *oauth2.Token) error
	Retrieve(int64, string) (*oauth2.Token, error)
}

type tokenStore struct {
	db *gorm.DB
}

func (s *tokenStore) CreateOrUpdate(
	userID int64,
	provider string,
	token *oauth2.Token,
) error {
	dbToken := Token{UserID: userID, Provider: provider}
	ok := s.db.Where(
		map[string]interface{}{"user_id": userID, "provider": provider},
	).First(&dbToken).RecordNotFound()
	dbToken.AccessToken = token.AccessToken
	dbToken.Expiry = token.Expiry.UnixNano()
	dbToken.RefreshToken = token.RefreshToken
	dbToken.TokenType = token.TokenType
	if ok {
		return s.db.Create(&dbToken).Error
	}
	return s.db.Save(&dbToken).Error
}

func (s *tokenStore) Retrieve(userID int64, provider string) (*oauth2.Token, error) {
	dbToken := &Token{UserID: userID, Provider: provider}
	if err := s.db.First(dbToken).Error; err != nil {
		return nil, err
	}
	token := &oauth2.Token{
		AccessToken:  dbToken.AccessToken,
		Expiry:       time.Unix(0, dbToken.Expiry),
		RefreshToken: dbToken.RefreshToken,
		TokenType:    dbToken.TokenType,
	}
	return token, nil
}
