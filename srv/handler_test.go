package main

import (
	"context"
	"fmt"
	"testing"
	"time"

	vcr "github.com/ad2games/vcr-go"
	oauth "gitlab.com/gitmate-micro/auth/srv/proto"
	"golang.org/x/oauth2"
)

type userProviderTuple struct {
	UserID   int64
	Provider string
}

type testTokenStore struct {
	Repository map[userProviderTuple]*Token
}

func (s *testTokenStore) CreateOrUpdate(
	userID int64,
	provider string,
	token *oauth2.Token,
) error {
	dbToken := &Token{
		AccessToken:  token.AccessToken,
		Expiry:       token.Expiry.UnixNano(),
		RefreshToken: token.RefreshToken,
		TokenType:    token.TokenType,
		UserID:       userID,
		Provider:     provider,
	}
	s.Repository[userProviderTuple{userID, provider}] = dbToken
	return nil
}

func (s *testTokenStore) Retrieve(userID int64, provider string) (*oauth2.Token, error) {
	dbToken, ok := s.Repository[userProviderTuple{userID, provider}]
	if !ok {
		return nil, fmt.Errorf("no matching entry")
	}
	token := &oauth2.Token{
		AccessToken:  dbToken.AccessToken,
		Expiry:       time.Unix(0, dbToken.Expiry),
		RefreshToken: dbToken.RefreshToken,
		TokenType:    dbToken.TokenType,
	}
	return token, nil
}

func TestGetAuthCodeURL(t *testing.T) {
	gh := &OAuth2{}
	resp := &oauth.AuthCodeURLResponse{}
	err := gh.GetAuthCodeURL(
		context.TODO(),
		&oauth.AuthCodeURLRequest{State: "blahblehblah", Provider: "github"},
		resp,
	)
	if err != nil {
		t.Error(err)
	}
	expectedURL := "https://github.com/login/oauth/authorize?client_id=" +
		"&response_type=code&scope=user&state=blahblehblah"
	if resp.Url != expectedURL {
		t.Errorf("unexpected URL, have: %s, want %s",
			resp.Url, expectedURL)
	}
}

func TestGetAuthCodeURLErrorUnknownProvider(t *testing.T) {
	gh := &OAuth2{}
	resp := &oauth.AuthCodeURLResponse{}
	err := gh.GetAuthCodeURL(
		context.TODO(),
		&oauth.AuthCodeURLRequest{State: "blahblehblah", Provider: "bleh"},
		resp,
	)
	if err == nil {
		t.Error("Expected error to be raised, but did not: " +
			"Unknown provider: bleh")
	}
	if err.Error() != "Unknown provider: bleh" {
		t.Errorf("Expected: %s, Got: %s", "Unknown provider: bleh", err.Error())
	}
}

func TestGetAuthCodeURLErrorEmptyStateParameter(t *testing.T) {
	gh := &OAuth2{}
	resp := &oauth.AuthCodeURLResponse{}
	err := gh.GetAuthCodeURL(
		context.TODO(),
		&oauth.AuthCodeURLRequest{State: "", Provider: "github"},
		resp,
	)
	if err == nil {
		t.Error("Expected error to be raised, but did not: " +
			"Empty state parameter")
	}
	if err.Error() != "Empty state parameter" {
		t.Errorf("Expected: %s, Got: %s", "Empty state parameter", err.Error())
	}
}

func TestGenerateTokenErrorUnknownProvider(t *testing.T) {
	gh := &OAuth2{}
	resp := &oauth.GenerateTokenResponse{}
	err := gh.GenerateToken(
		context.TODO(),
		&oauth.GenerateTokenRequest{Code: "blahblehblah", Provider: "bleh"},
		resp,
	)
	if err == nil {
		t.Error("Expected error to be raised, but did not: " +
			"Unknown provider: bleh")
	}
	if err.Error() != "Unknown provider: bleh" {
		t.Errorf("Expected: %s, Got: %s", "Unknown provider: bleh", err.Error())
	}
}

func TestGenerateTokenErrorEmptyCodeParameter(t *testing.T) {
	gh := &OAuth2{}
	resp := &oauth.GenerateTokenResponse{}
	err := gh.GenerateToken(
		context.TODO(),
		&oauth.GenerateTokenRequest{Code: "", Provider: "github"},
		resp,
	)
	if err == nil {
		t.Error("Expected error to be raised, but did not: " +
			"Empty code parameter")
	}
	if err.Error() != "Empty code parameter" {
		t.Errorf("Expected: %s, Got: %s", "Empty code parameter", err.Error())
	}
}

func TestStoreToken(t *testing.T) {
	vcr.Start("store.token", nil)
	defer vcr.Stop()

	store := &testTokenStore{map[userProviderTuple]*Token{}}
	gh := &OAuth2{store}

	err := gh.storeToken(context.TODO(), &oauth2.Token{
		AccessToken: "somerandomtokenthatworksbecauseofvcr-go",
	}, "github")
	if err != nil {
		t.Error(err)
	}

	token, err := store.Retrieve(17202890, "github")
	if err != nil {
		t.Error(err)
	}
	if token.AccessToken != "somerandomtokenthatworksbecauseofvcr-go" {
		t.Errorf("stored token does not match; have: %s, want: %s",
			token.AccessToken, "somerandomtokenthatworksbecauseofvcr-go")
	}
}
