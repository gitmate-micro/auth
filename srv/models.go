package main

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// Token represents the ORM for token database.
type Token struct {
	gorm.Model
	AccessToken  string
	Expiry       int64
	RefreshToken string
	TokenType    string
	Provider     string `gorm:"unique_index:idx_provider_user"`
	UserID       int64  `gorm:"unique_index:idx_provider_user"`
}

func connectToDB() (*gorm.DB, error) {
	user := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")
	port := os.Getenv("DB_PORT")
	dbname := os.Getenv("DB_NAME")
	host := os.Getenv("DB_HOST")

	return gorm.Open(
		"postgres",
		fmt.Sprintf(
			"user=%s password=%s port=%s dbname=%s host=%s sslmode=disable",
			user, password, port, dbname, host,
		),
	)
}
