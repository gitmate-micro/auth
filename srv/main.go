//+build !test

package main

import (
	"github.com/micro/go-log"
	"github.com/micro/go-micro"
	oauth "gitlab.com/gitmate-micro/auth/srv/proto"
)

func main() {
	db, err := connectToDB()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&Token{})
	store := &tokenStore{db}
	handler := &OAuth2{store}

	// New Service
	service := micro.NewService(
		micro.Name("gitmate.micro.auth.srv"),
		micro.Version("latest"),
	)

	// Register Handler
	oauth.RegisterAuthServiceHandler(service.Server(), handler)

	// Initialise service
	service.Init()

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
